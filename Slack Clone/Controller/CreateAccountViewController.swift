//
//  CreateAccountViewController.swift
//  Slack Clone
//
//  Created by Takasur Azeem on 28/04/2018.
//  Copyright © 2018 Takasur Azeem. All rights reserved.
//

import UIKit

class CreateAccountViewController: UIViewController {
    // Mark:- Outlets
    @IBOutlet weak var userNameText: UIStackView!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var userImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func createAccountPressed(_ sender: UIButton) {
        guard let email = emailText.text , emailText.text != "" else { return }
        guard let password = passwordText.text , passwordText.text != "" else { return }
        
        AuthService.instance.registerUser(with: email, and: password) { (success) in
            if success {
                AuthService.instance.loginUser(with: email, and: password, completion: { success in
                    if success {
                        print("Logged in user!", AuthService.instance.authToken)
                    }
                })
            }
        }
        
    }
    
    @IBAction func pickAvatarPressed(_ sender: UIButton) {
    }
    
    @IBAction func pickBGColorPressed(_ sender: UIButton) {
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

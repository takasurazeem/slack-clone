//
//  LoginViewController.swift
//  Slack Clone
//
//  Created by Takasur Azeem on 25/04/2018.
//  Copyright © 2018 Takasur Azeem. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createAccountButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: TO_CREATE_ACCOUNT, sender: self)
    }
    
    
    @IBAction func loginButtonPressed(_ sender: RoundedButton) {
        guard let email = userNameText.text , userNameText.text != "" else { return }
        guard let password = passwordText.text , passwordText.text != "" else { return }
        
        AuthService.instance.registerUser(with: email, and: password) { (success) in
            if success {
                print("user logged in!")
            }
        }
    }
}

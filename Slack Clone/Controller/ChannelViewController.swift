//
//  ChannelViewController.swift
//  Slack Clone
//
//  Created by Takasur Azeem on 18/04/2018.
//  Copyright © 2018 Takasur Azeem. All rights reserved.
//

import UIKit
import SideMenu
import Foundation

class ChannelViewController: UIViewController {
    
    // Mark: - Outlets
    @IBOutlet weak var loginButton: UIButton!
    
    
    internal var appScreenRect: CGRect {
        let appWindowRect = UIApplication.shared.keyWindow?.bounds ?? UIWindow().bounds
        return appWindowRect
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isTranslucent = true
        if let navigationController =  self.navigationController as? UISideMenuNavigationController {
            navigationController.sideMenuManager.menuAnimationBackgroundColor = UIColor.clear
            navigationController.sideMenuManager.menuWidth = appScreenRect.width * 0.75
        }
    }

    @IBAction func loginButtonPressed(_ sender: UIButton) {
        print("attempting to perform segue")
        performSegue(withIdentifier: TO_LOGIN, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

//
//  Constant.swift
//  Slack Clone
//
//  Created by Takasur Azeem on 25/04/2018.
//  Copyright © 2018 Takasur Azeem. All rights reserved.
//

import Foundation

typealias CompletionHander = (_ Success: Bool) -> ()

// URL Constants
let BASE_URL = "https://takasurs-smack-app.herokuapp.com/v1/"
let URL_REGISTER = "\(BASE_URL)account/register"
let URL_LOGIN = "\(BASE_URL)account/login"

// Mark: - Segues
let TO_LOGIN = "toLogin"
let TO_CREATE_ACCOUNT = "toCreateAccount"

// Mark: - User Defaults
let TOKEN_KEY = "token"
let LOGGED_IN_KEY = "loggedIn"
let USER_EMAIL = "userEmail"


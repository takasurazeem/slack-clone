//
//  GradientView.swift
//  Slack Clone
//
//  Created by Takasur Azeem on 24/04/2018.
//  Copyright © 2018 Takasur Azeem. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {

    @IBInspectable public var topColor: UIColor = #colorLiteral(red: 0.2926680446, green: 0.3262870312, blue: 0.8760043979, alpha: 1) {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable public var bottomColor: UIColor = #colorLiteral(red: 0.1725490196, green: 0.831372549, blue: 0.8470588235, alpha: 1) {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
     }

    override init(frame: CGRect) {
        super.init(frame: frame)
     }
    
    override func layoutSubviews() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
